# Diaporama en Markdown (reveal.js)

Ce modèle vous permet de créer des diaporamas facilement en markdown ([Exemple de diaporama](https://docs.forge.apps.education.fr/modeles/diaporama-en-markdown/)).

## Utilisation de ce modèle

### Initialisation

- Cliquez sur le bouton "Créer une divergence" en haut à droite, modifiez éventuellement les paramètres puis cliquez sur "Créer un projet divergent"

- Modifiez le fichier a-propos.md pour y rajouter votre nom.
  
    - Cliquez sur le fichier `a-propos.md`.

    - Cliquez sur le bouton Editer (<img src="/uploads/a9d6fb815c18d8a521b02f50fa13ccbd/image.png" height="24px">). Si le bouton bleu indique un autre texte, utilisez la petite flèche à droite pour sélectionner "Editer").

    - Effectuez vos modifications.

    - Sauvegardez en cliquant sur le bouton "Valider les modifications".

### Accèder au diaporama

- Dans le menu de gauche, cliquez sur "Paramètres > Pages".

- Cliquez ensuite sur le lien (vous devez avoir modifié un fichier au moins une fois et avoir patienté 1 minute afin que le lien soit présent).

### Création de votre diaporama

Une fois que vous avez accédé à votre diaporama, vous pouvez très facilement le modifier.

Ouvrez le menu de gauche grâce au bouton composé de trois traits horizontaux (en bas à gauche), sélectionnez l'onglet "Outils" puis cliquez sur "Modifier la présentation".

Vous accéderez ainsi directement sur la page sur la forge permettant de modifier le fichier `diaporama.md`.

Le contenu doit être rédigé en markdown (vous pouvez utiliser les boutons de l'éditeur pour aller plus vite) et chaque diapositive doit être séparée par trois tirets (`---`).

## Informations complémentaires

### Le menu

Vous pouvez utiliser le bouton ![image](/uploads/5142f2a123274fb56533289d37b6cf00/image.png) situé en bas à gauche pour ouvrir un menu. Ce menu offre de nombreuses fonctionnalités :

- L'onglet **slide** vous permet de voir le plan de votre diaporama et de passer rapidement d'une diapositive à une autre.

- L'onglet **outils** contient de nombreux liens pratiques :

    - **Version imprimable :** Par défaut, si vous essayez d'imprimer votre diaporama via le navigateur (grâce au raccourci `Ctrl+P` par exemple), le résultat ne sera pas optimal. Activez d'abord la version imprimable puis faites `Ctrl+P` afin d'obtenir un meilleur résultat. Pratique pour exporter votre présentation au format PDF !

    - **Modifier la présentation :**  Cliquez sur ce lien pour ouvrir directement la page d'édition du fichier `diaporama.md`. Vous devrez être connecté à votre compte sur la forge.

    - **Voir le code source de la présentation :** Permet d'obtenir (sur la forge) le code markdown ayant permis de générer la présentation .

    - **Afficher le plan :** Affiche les miniatures de toutes vos diapositives et vous permet d'y accéder en cliquant dessus. Cette vue peut également être ouverte en appuyant sur `Echap` ou sur la touche `O` (pensez **O**mniscient).

    - **Afficher/Masquer le tableau blanc :** Vous permet d'obtenir un tableau blanc sur lequel vous pouvez écrire. Chaque tableau blanc est associé à une diapositive réelle. Le contenu du tableau blanc est stocké temporairement dans le navigateur et sera présent dans la version imprimable. Cette vue peut également être ouverte/fermée via la touche `B` (pensez tableau **B**lanc).

    - **Vue présentateur :** Ouvre une fenêtre qui permet de contrôler la présentation et vous affiche la diapositive suivante ainsi que les notes de la diapositive actuelle (si il y en a). C'est très pratique lorsque vous avez un double écran. Cette vue peut également être ouverte via la touche `S` (pensez **s**econd écran, ou **s**uperviser, ou **s**peaker view en anglais)

    - **Télécharger la présentation hors ligne :** (à venir)

- L'onglet **A propos** affiche le contenu du fichier `a-propos.md`.

- L'onglet **Thèmes** vous permet de changer le thème.

### Le crayon

Le bouton crayon (en bas à gauche) permet d'annoter la diapositive actuelle. Le contenu est stocké temporairement dans votre navigateur. Ce mode peut être activé / désactivé via la touche `C` (pensez **c**rayon). Pour des raisons techniques, ces annotations ne seront pas disponibles sur la version imprimable. Pour effacer un trait, utilisez le clic droit (ou maintenez enfoncé sur un écran tactile). Pour effacer toutes les annotations de la diaposative actuelle, utilisez la touche `Suppr`.

### Écrire des mathématiques

Il est possible d'insérer des formules mathématiques rédigée en LaTeX.

Exemple :
```markdown
On a $\ln{1}=0$. De plus : $$\ln{a \times b} = \ln{a} + \ln{b}$$.
```

Le simple dollar permet d'insérer une formule en mode condensée sans revenir à la ligne.

Le double dollar permet d'insérer une formule en gros, centrée, et sur une nouvelle ligne (correspond à `\[` et `\]` en LaTeX).

### Diapositive verticale

En général, les diapositives défilent horizontalement (on passe à la diapositive suivante avec la touche droite). Il est également possible de rajouter des diapositives verticalement (on passera alors à la diapositive suivante avec la touche bas). Il suffit pour cela de séparer le contenu markdown par quatre traits au lieu de trois (`----`). Pratique pour traiter des sous-sujets par exemple.

### Les notes (vue présentateur)

Vous pouvez ajouter des notes à la fin de votre diapositive grâce au mot clef `Note:`. Ces notes seront affichées dans la vue présentateur accessible via le menu.

```markdown
En première, l'élève doit choisir trois spécialités. 
Note:
- Histoire-géographie, géopolitique et sciences politiques
- Humanités, littérature et philosophie
- Langues, littératures et cultures étrangères
- Mathématiques
- Physique-chimie
- Sciences de la vie et de la Terre
- Sciences économiques et sociales

---

En terminale, l'élève doit abandonner une de ses trois spécialités pour en conserver deux.
Note: Spé abandonnée coeff 8 en CC, spé conservées coeff 16 chacun en éval terminale

```

### RevealJs

Cet outil a été développé à partir de [RevealJs](https://revealjs.com/). N'hésitez pas à consulter la documentation pour découvrir d'autres fonctionnalités avancées. Liste des plugins installés : RevealMarkdown, RevealMenu, RevealChalkboard, RevealCustomControls, RevealNotes, RevealMath.KaTeX.


## Licence

Ce modèle est sous licence [MIT](https://opensource.org/license/mit/). Cette licence ne concerne que ce modèle et ne concerne pas le contenu du diaporama (fichier `diaporama.md`). N'hésitez pas à consulter le contenu du fichier `a-propos.md` pour connaître la licence du contenu du diaporama (dans le cas où l'auteur en a choisi une).

## Fonctionnalités avancées

### Mermaid.js
Il est possible d'insérer des diagrames mermaid.js :
````html
<div class="mermaid">
flowchart LR
    2nd[Seconde]
    1G[Première Générale]
    TG[Terminale Générale]
    1T[Première Technologique]
    TT[Terminale Technologique]
    2nd --> 1G
    2nd --> 1T
    1G --> TG
    1T --> TT
</div>
```
