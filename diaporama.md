# Diaporama

Ceci est exemple de diaporama. Cliquez sur la flèche de droite (en bas à droite) pour passer à la diapositive suivante. Vous pouvez aussi appuyer sur la touche espace ou sur la flèche droite de votre clavier.

---
## Exemple de diaporama


Le contenu du diaporama est rédigé en markdown. Un langage textuel simple et facile à utiliser pour mettre en forme du texte.

On suppose que vous connaissez déjà, sinon, rendez-vous [à une courte présentation de Markdown](#quelques-rudiments-de-markdown)
---


## Fonctionnement 🛠️

---
### Syntaxe 📓

Chaque diapositive est séparée par trois tirets (`---`).

--- 

Attention, la séquence `---‗` avec un espace après produira une ligne de séparation, cf supra.

---

### Liens entre les pages 📃

On peut accéder à une diapositive directement par son numéro de page. Ajouter [#/4](#/4) vous enverra à la page 4.

---

Il est possible de prévoir des approfondissements ou de structurer sa présentation en deux dimensions. On utilise alors `----` comme séparateur.

Attention la barre d'espace vous emmène  sur cette page.

----

Dans ce cas, la page est numérotée [#/5/1](#/5/1)

----

![](imgs/vue_grille.png)

----

Les pages avec un titre peuvent être liées par l'identifiant, par exemple
`[Page courante](#liens-entre-les-pages)` ou par `[Dernière page](#the-end)`

---

Les images doivent normalement s'adapter à la vue en cours, mais si vous devez les redimensionner, il suffit de rajouter
`<!-- .element: style="width: 15%">` par exemple.

Pour changer la taille du texte, on peut utiliser
`<!-- .element style="font-size: 45%" -->` derrière l'élément ou une balise HTML, comme `<span>`.

---

### Apparitions progressives ▶️

Il est possible de 

faire apparaitre <!-- .element: class="fragment highlight-red"-->

des éléments avec

`<!-- .element: class="fragment"-->` <!-- .element: class="fragment"-->

----

Pour le faire en ligne, il faut utiliser <span class="fragment">`<span class="fragment">…</span>`</span>

---

### Les colonnes 📰

<div class="container">

<div class="col" >
Pour avoir du texte sous plusieurs colonnes, il faut utiliser du HTML
</div>


<div class="col">

```html
<div class="container">
<div class="col">
Col1 
</div>

<div class="col" >
Col2
</div>
</div>
```
</div>
</div>

---

### Contenu multimedia 🎬

Avec une iframe depuis Tube

```html
<iframe title="Mercredi du Numérique - Épisode 1 - #LaForgeEdu : Une initiative nationale pour les communs numériques éducatifs" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/a3d2e178-2a83-4966-9fc7-959705fcdfed" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
```

<iframe title="Mercredi du Numérique - Épisode 1 - #LaForgeEdu : Une initiative nationale pour les communs numériques éducatifs" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/a3d2e178-2a83-4966-9fc7-959705fcdfed" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

----

Code complet de l'iframe

```html
<iframe 
title="Mercredi du Numérique - Épisode 1 - #LaForgeEdu : Une initiative nationale pour les communs numériques éducatifs" 
width="560" height="315" 
src="https://tube-sciences-technologies.apps.education.fr/videos/embed/a3d2e178-2a83-4966-9fc7-959705fcdfed" 
frameborder="0" allowfullscreen="" 
sandbox="allow-same-origin allow-scripts allow-popups allow-forms">
</iframe>
```
----

S'il y a de nombreux contenus multimédias, on peut les «précharger»

```html
 <img data-src="image.png">
  <iframe data-src="https://hakim.se"></iframe>
  <video>
    <source data-src="video.webm" type="video/webm" />
    <source data-src="video.mp4" type="video/mp4" />
</video>
```

---- 

Les vidéos en direct (à éviter)

```html
<video>
<source src="https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4" />
</video>
```

<video>
<source src="https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4" />
</video>

---

### Le fond 📊
<!-- .slide: data-background="#ff0000" -->

```markdonw
<!-- .slide: data-background="#ff0000" -->
```

----

Ou une image <!-- .element: style="text-align: left" -->

```markdown
<!-- .slide: data-background="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/brigit-et-komit/Brigit_et_Komit_avec_fond_forge_et_logo_compact_L.png" -->
```

<!-- .slide: data-background="https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/brigit-et-komit/Brigit_et_Komit_avec_fond_forge_et_logo_compact_L.png" -->

---

### Le menu 🍔

<div class="container">
<div class="col">
Il est possible de naviguer directement dans les diapos avec le menu situé en bas à gauche.
</div>

<div class="col" >
<img src="imgs/menu.png"/>
</div>
</div>

---

### Quelques outils 🔧

<div class="container">
<div class="col">
Quelques outils ont été rajoutés en lien direct.
</div>

<div class="col" >
<img src="imgs/outils.png"/>
</div>
</div>

---

### Raccourcis 🩳

- `V` remplace la présentation par un écran noir
- `B` remplace la présentation par un tableau interactif
- `O` vous présente toutes les diapostives
- `S` vous présente la vue «présentateur»

notes:
Présenter la vue «présentateur»
----

### Vue présentateur 📡

![](imgs/presentateur+notes.png)

notes: 
utiles pour être sur de ne pas oublier un point essentiel

----

* nécessite de ne pas être en écrans dupliqués (ou clones)
* permet d'avoir un chronomètre et l'heure
* permet de voir la diapo suivante
* permet de voir des notes sur le contenu
    * Ces notes sont le texte situé après <code>notes&#58;</code>

---

### Fichier `a-propos.md`

<div class="container">
<div class="col">

Le fichier `a-propos.md` permet d'indiquer votre nom et la licence du document.
</div>

<div class="col" >
<img src="imgs/a-propos.png"/>
</div>
</div>

---

### Logo (et favicon)

Pour changer le logo de votre projet, ainsi que la favicon de votre diapo, il suffit de changer le fichier `logo.png`.

---

### VSCodium 💻

Deux extentions :
+ "evilz.vscode-reveal"
+ "bierner.emojisense"

---

### Approfondissements

Ce document ne pourrait être possible sans

[reveal.js](https://revealjs.com/)

qui vous servira de référence.

Le [dépot](https://forge.apps.education.fr/docs/modeles/diaporama-en-markdown) peut-être [bifurqué](https://forge.apps.education.fr/docs/modeles/diaporama-en-markdown/-/forks/new) pour créer votre propre présentation.

---

## Quelques rudiments de Markdown

---

### Les titres

```markdown
# Titre général
```

<span style="font-size: var(--r-heading1-size);font-weight: bold;text-transform: uppercase;">Titre général</span>

```markdown
## Partie
```

<span style="font-size: var(--r-heading2-size);font-weight: bold;text-transform: uppercase;">Partie</span>

```markdown
### Sous partie
```

<span style="font-size: var(--r-heading3-size);font-weight: bold;text-transform: uppercase;">Sous partie</span>

---

### Énumération

```markdown
- une
- liste
- à
- puces
```

- une
- liste
- à
- puces


---

### Énumération ordonnée

```markdown
1. une
3. liste
1. numérotée
```

1. une
3. liste
2. numérotée

---

### Emphase

`**gras**`

**gras**

`*italique*`

*italique*

---
### Liens

`[Wikipedia](https://fr.wikipedia.org/)` 

[Wikipedia](https://fr.wikipedia.org/)

---

### Images

`![acceuil](imgs/Adelie_Penguins_on_iceberg.jpg)`

![acceuil](imgs/Adelie_Penguins_on_iceberg.jpg) <!-- .element width="25%" -->
Jason Auch, CC BY 2.0 <https://creativecommons.org/licenses/by/2.0>, via Wikimedia Commons


---

### Des extraits de code


````markdown
```python
def reponse():
    return 42
```
````
</div>

<div class="col" >

```python
def reponse():
    return 42
```
</div>
</div>

---

### Équations


````latex
$$ {F}_{A/B} = G \times \frac{M_A M_B}{d^2} $$
````


$$ {F}_{A/B} = G \times \frac{M_A M_B}{d^2} $$

```latex
$\ce{Hg^2+ ->[I-] HgI2 ->[I-] [Hg^{II}I4]^2-}$
```

$\ce{Hg^2+ ->[I-] HgI2 ->[I-] [Hg^{II}I4]^2-}$


